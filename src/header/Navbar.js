import React from 'react';
import { Link, withRouter } from 'react-router-dom';

const Navbar = () => {
	return (
		<nav>
			<div className='logo'>
				<Link to='/'>Netflix Clone</Link>
			</div>
			<div style={{ clear: 'both' }} />
		</nav>
	);
};

export default withRouter(Navbar);
